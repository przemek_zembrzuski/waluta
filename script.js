function kurs() {
    function getData(url) {
        return new Promise(function(resolve, reject) {
            var http = new XMLHttpRequest();
            http.open("GET", url, true);
            http.onload = function() {
                if (http.status == 200) {
                    resolve(JSON.parse(http.response));
                } else {
                    reject(http.statusText);
                }
            };
            http.onerror = function() {
                reject(http.statusText)
            }
            http.send();
        })

    }

    var code = document.getElementById('code');
    var val = document.getElementById('val');
    if(isNaN(val.value)){
      document.getElementById("kurs").innerHTML = "Podaj liczbę";
    }else{
      var pro = getData("http://api.nbp.pl/api/exchangerates/rates/a/" + code.value + "/?format=json");
      pro.then(function(data) {
          var kurs = data.rates[0].mid;
          var takeTo = kurs * val.value;
          document.getElementById("kurs").innerHTML = takeTo.toFixed(2)+" PLN";
          val.value = "";
      });
    }
}
